.PHONY: synth deploy destroy

synth:
	npx aws-cdk --app "npx ts-node --prefer-ts-exts cdkshare.ts" synth --all

deploy:
	npx aws-cdk --app "npx ts-node --prefer-ts-exts cdkshare.ts" deploy --all


destroy:
	npx aws-cdk --app "npx ts-node --prefer-ts-exts cdkshare.ts" destroy --all

