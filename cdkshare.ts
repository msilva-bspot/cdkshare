/ * 
  * create a vpc and share it
  *
* /
import * as cdk from 'aws-cdk-lib';
import * as ec2 from 'aws-cdk-lib/aws-ec2'


const AWS_DEFAULT_REGION = process.env.AWS_DEFAULT_REGION || 'us-east-1'
const AWS_DEFAULT_ACCOUNT = process.env.AWS_DEFAULT_ACCOUNT || '00000000000'
const AWS_KEYPAIR = process.env.AWS_KEPAIR || 'me-us-east-1'

/* create the shared VPC */
class Stack1 extends cdk.Stack {
  public readonly vpc: ec2.Vpc;

  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    this.vpc = new ec2.Vpc(this, 'SharedVPC');
  }
}

/* create new props struct to share */
interface SharedProps extends cdk.StackProps {
  vpc: ec2.IVpc;
}

/** consumes the shared VPC */
class Stack2 extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props: SharedProps) {
    super(scope, id, props);

    new ec2.SecurityGroup(this, 'SecurityGroupWithSharedVpc', {
      vpc: props.vpc,
    });

  }
}

const app = new cdk.App()

const stack1 = new Stack1(app, 'Stack1');
const stack2 = new Stack2(app, 'Stack2', {
  vpc: stack1.vpc,
});

app.synth();
